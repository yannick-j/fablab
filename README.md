# FabLab

## Description
Documentation et projets personnels au FabLab LabBoîte à Cergy.

## Sommaire

### Projet d'atelier : Que faire d'un vieil ordinateur?

Dossier : [/atelier_que_faire_vieil_ordi/](/atelier_que_faire_vieil_ordi/)

- Notes de préparation : `atelier_fablab.pdf`
- Diaporama de présentation : `slides.pdf`
- Document public : `documentation.pdf`
