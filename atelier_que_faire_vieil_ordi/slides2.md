---
title: "Que faire d'un vieil ordinateur?"
author : "Yannick, Amaury, Djamel"
institute: "LabBoîte, Cergy-Pontoise Campus"
date: "Mercredi 19 octobre 2022"
theme: "Singapore"
colortheme: "default"
aspectratio: 169
colorlinks: true
header-includes:
- \usepackage{caption}
- \captionsetup[figure]{labelformat=empty}
---

# Que faire d'un vieil ordinateur?

![\ ](images/computer_recycle.jpg){width=200px}

![\ ](images/logo-labboite.png){width=100px}

---

## Un atelier pour :

apprendre à installer

- un système d'exploitation libre, léger et modulaire

\

![GNU/Linux](images/GNU_and_Tux.png){width=100px}

- et des logiciels libres

pour réutiliser un ordinateur obsolète en vue d'un usage précis.

---

## INTRODUCTION

### Constat :

\

- Obsolescence trop rapide des machines,
- des déchets électroniques polluants, difficilement recyclables,
utilisant des matières premières parfois rares,

![\ ](images/dechets_electroniques.jpg){width=100px}

- une consommation électrique trop importante,
- un coût trop important.

---

### Causes :

- Des systèmes d'exploitation trop gourmands en ressources,

\

![\ ](images/pc_escargot.jpg){width=100px}

- des appareils connectés à internet
qui nécessitent des mises à jour de sécurité constantes,
- des systèmes d'exploitation et des logiciels peu configurables
et donc peu adaptables aux besoins réels,
- des logiciels propriétaires payants à chaque mise à jour.

---

### Remèdes :

- Identifier le besoin pour optimiser un ordinateur pour un usage précis,
- utiliser un système d'exploitation et des logiciels libres
- configurer le système et les logiciels pour réduire les besoins en
puissance de calcul et en consommation d'énergie.

\

![Follow the White Rabbit by FabledCreative <https://www.deviantart.com/fabledcreative/art/Follow-the-White-Rabbit-742097389>](images/follow_the_white_rabbit_by_fabledcreative.jpg){width=100px}

---

### Limites :

- Compétences,
- temps d'apprentissage et de réalisation,
- limitations techniques dues aux capacités des ordinateurs de récup.

\

![\ ](images/prise_de_tete.jpg){width=200px}

---

## SCÉNARIOS POSSIBLES

\

![media-center](images/kodi_800.jpg){width=160px}
![bureautique](images/mac-mini_800.jpg){width=160px}
![prise de notes](images/eeepc_800.jpg){width=160px}

---

### Un media-center

![\ ](images/kodi_800.jpg){width=180px}

- **Objectif** : un ordinateur branché à un téléviseur, pour regarder des
films, photos, écouter de la musique, des radios en ligne, des podcasts,
la TV sur ip, des streams,... 
- **machine de récup** : un portable dont l'écran est HS
- **OS** : LibreELEC

![\ ](images/libreelec.png){width=50px}

---

### Un poste bureautique léger

\

![\ ](images/mac-mini_800.jpg){width=180px}

- **Objectif** : un ordinateur connecté à internet pour la bureautique.

- **machine de récup** : un mac-mini avec écran

- **OS** : debian + xfce, lxde, lxqt ou mate

- **logiciels** : libreoffice

---

### Un poste nomade pour la prise de notes

\

![\ ](images/eeepc_800.jpg){width=180px}

- **Objectifs** : Prise de notes.

- **machine de récup** : un netbook.

- **OS** : debian + gestionnaire de fenêtres (openbox, i3).

- **logiciels** : éditeur Markdown (ghostwriter ou ReText), visualiseur de pdf (evince), pandoc pour convertir les formats de fichier.

---

## RÉFLÉCHIR AVANT DE COMMENCER

![\ ](images/idee.jpg){width=200px}

- Besoins;
- ressources;
- objectifs du projet;
- Compétences et budget.

---

## SE DOCUMENTER

- choisir une distribution GNU/Linux : <https://fr.wikipedia.org/wiki/Liste_des_distributions_GNU/Linux>

![Distributions GNU/Linux](images/distro.jpg){width=100px}

- choisir ses logiciels : <https://framalibre.org/>

![\ ](images/framalibre.png){width=200px}

- documentation (arch wiki)

---

## INSTALLER, TESTER, CONFIGURER

- Télécharger une iso d'installation au format live-cd pour tester la reconnaissance du matériel
- flasher cette iso sur un support usb
- booter et tester
- Télécharger l'iso d'installation choisie
- flasher l'iso sur un support usb
- booter sur le support usb et suivre les étapes de l'installateur
- faire les mises à jour
- installer les logiciels
- configurer

---

## MISE EN ŒUVRE COLLECTIVE D'UN DES SCÉNARIOS : un poste bureautique léger

\

**Objectif** : Un ordinateur pour utiliser des outils bureautiques (traitement de texte, tableur,présentation de diapositives, etc).

![](images/lubuntu_libreoffice.png){width=200px}

---

#### MATÉRIEL :
Une machine virtuelle : Oracle VM VirtualBox 6.1 sous Windows 10

#### LOGICIELS :
Distribution Lubuntu 22.04.1 LTS (Jammy Jellyfish) pour architecture 64bits (amd64)

\

![Lubuntu](images/lubuntu_logo.png){width=100px}

---

#### INSTALLATION :

Si on avait été dans sur une machine physique.

- Télécharger l'iso d'installation de la distribution Lubuntu 22.04.1 LTS (Jammy Jellyfish) pour architecture 64bits (amd64) : <https://lubuntu.me/downloads/>
- Lancer Etcher pour flasher cette image sur une clef usb

Mais on est sur une machine virtuelle sans droits d'administration.

- Démarrer la machine virtuelle et appuyer sur F12 pour démarrer sur l'iso;
- suivre les instructions de l'installateur.

---

## CONCLUSION : POUR ALLER PLUS LOIN

---

- Utiliser les ressources du FabLab pour améliorer, personnaliser

![Une borne d'arcade à partir d'un vieil ordinateur portable :](images/arcade.jpg){width=250px}

<https://www.pinterest.fr/pin/58054282668328134>

---

- Utiliser un ordinateur à carte unique.

![Lecteur DVD + pi zero + kodi](images/DVDplayer.jpg){width=250px}
<https://www.hackster.io/news/imgurian-converts-an-old-portable-dvd-player-into-a-raspberry-pi-media-center-f3984ffe199c>

---

![Raspberry pi 3 + tft 3,5" + boitier + octoprint ](images/OctoPrint-Raspberry-Pi-Rig-3.5.jpg){width=250px}

<https://learn.adafruit.com/3-dot-5-pitft-octoprint-rig/overview>

---

## Sitographie

- Distribution LibreELEC : <https://libreelec.tv/>
- Documentation Kodi : <https://kodi.tv/> et <https://kodi.wiki/view/Main_Page>
- Annuaire francophone de logiciels libres : <https://framalibre.org/>
- Liste des distributions Gnu/Linux : <https://fr.wikipedia.org/wiki/Liste_des_distributions_GNU/Linux>
- Manuel d'installation Debian : <https://d-i.debian.org/manual/fr.amd64/apa.html> (guide rapide) et <https://d-i.debian.org/manual/> (plus complet, par langue et architecture).
- Wiki de la distributgdho Arch : <https://wiki.archlinux.org>
