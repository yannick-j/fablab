# Que faire d'un vieil ordinateur?
Atelier FabLab : Réutiliser un ordinateur obsolète

- **Objectifs** :
apprendre à installer un système d'exploitation libre, léger et modulaire et des logiciels libres pour réutiliser un ordinateur obsolète en vue d'un usage précis.

- **Public visé** :
adulte familier avec l'usage des outils informatiques (formater un disque, installer un logiciel, modifier des réglages système).

- **Matériel nécessaire** :
vieux ordinateurs de récupération, écran de projection, machines virtuelles

- **Intervenants** :
Yannick Juino, Amaury Nieduziak, Djamel Benkara
- Nombre de personnes : 12 max
- Durée : deux heures
- Espace : postes de conception
- Date : mercredi 19 octobre 2022


## INTRODUCTION


### Constat :


- Obsolescence trop rapide des machines,
- des déchets électroniques polluants, difficilement recyclables, des utilisant des matières premières parfois rares,
- une consommation électrique trop importante,
- un coût trop important.


### Causes :

- Des systèmes d'exploitation trop gourmands en ressources,
- des usages multiples qui entraînent des besoins très différents,
- des appareils connectés à internet qui nécessitent des mises à jour de sécurité constantes,
- des systèmes d'exploitation et des logiciels peu configurables et donc adaptables aux besoins réels,
- des logiciels propriétaires payants à chaque mise à jour.

### Remèdes :

- Identifier le besoin pour optimiser un ordinateur pour un usage précis,
- utiliser un système d'exploitation et des logiciels libres
- configurer le système et les logiciels pour réduire les besoins en puissance de calcul et en consommation d'énergie.

### Limites :

- Compétences,
- temps d'apprentissage et de réalisation,
- des limitations techniques dues aux capacités des ordinateurs de récup.

## SCÉNARIOS POSSIBLES

### Un media-center

**Objectif** : un ordinateur branchée à un téléviseur, connecté à internet et à un support de stockage de masse (disque dur de forte capacité, NAS), éventuellement un système hifi, pour regarder des films, photos, écouter de la musique, des radios en ligne, des podcasts, la TV sur ip, des streams,... 
machine de récup : un portable dont l'écran est HS

**OS** : libreelec

**logiciel** : kodi


### Un poste bureautique, pédagogique et ludique sécurisé pour un enfant

**Objectif** : un ordinateur  connecté à internet pour découvrir l'informatique, faire des jeux éducatifs, écouter de la musique, des livres audio et surfer en sécurité avec un temps d'écran limité.

**machine de récup** : un ordinateur avec écran

**OS** : debian + xfce

**logiciels** : gcompris, abiword, libreoffie, CTparental (https://www.numetopia.fr/installation-de-ctparental-et-configuration/), blacklist université de Toulouse.

### Un poste nomade pour la prise de notes

**Objectifs** : Prise de notes, mails et accès internet

**machine de récup** : un netbook, une tablette se connectant à un serveur en vnc

**OS** : debian + openbox

**logiciels** : éditeur markdown (ghostwriter), navigateur (firefox ou chromium), pandoc pour convertir les formats de fichier.

## COMMENT FAIRE ?

- Cahier des charges du projet
- Ressources
- Besoins
- Compétences et budget.

## DOCUMENTATION EN LIGNE

- choisir une distribution GNU/Linux (wikipedia)
- documentation (arch wiki)
- choisir ses logiciels (framasoft)

## INSTALLER, TESTER, CONFIGURER

- Télécharger une iso d'installation au format live-cd pour tester la reconnaissance du matériel
- flasher cette iso sur un support usb
- booter et tester
- Télécharger l'iso d'installation choisie
- flasher l'iso sur un support usb
- booter sur le support usb et suivre les étapes de l'installateur
- faire les mises à jour
- installer les logiciels
- configurer

## MISE EN ŒUVRE COLLECTIVE D'UN DES SCÉNARIOS

## CONCLUSION : POUR ALLER PLUS LOIN

- Utiliser les ressources du FabLab pour améliorer, personnaliser
- Modifier le matériel
- Utiliser un ordinateur à carte unique, un microcontrôleur.

https://www.instructables.com/Build-your-own-Mini-Arcade-Cabinet-with-Raspberry-/

NAS : https://restez-curieux.ovh/2021/11/05/creer-un-nas-avec-un-vieil-ordinateur-et-openmediavault/

## Sitographie

- Distribution LibreElec : https://libreelec.tv/
- Informations sur démarrage Kodi  : https://kodi.wiki/view/HOW-TO:Autostart_Kodi_for_Linux
- https://github.com/graysky2/kodi-standalone-service
- Annuaire francophone de logiciels libres : https://framalibre.org/
- Liste des distributions Gnu/Linux : https://fr.wikipedia.org/wiki/Liste_des_distributions_GNU/Linux
- Manuel d'installation Debian : https://d-i.debian.org/manual/fr.amd64/apa.html (guide rapide) et https://d-i.debian.org/manual/ (plus complet, par langue et architecture).

### EeePC 701SD : Un poste nomade pour la prise de notes

#### OBJECTIF :
Un poste nomade léger, à coût très bas, de grande autonomie électrique, permettant la prise de notes dans un format libre et structuré, y compris pour les formules mathématiques. Public étudiant.

#### MATÉRIEL :
Asus Eeepc 701 SD, 2008.

- Coût d'origine 200€, cote en occasion 50€.
- CPU : Intel Celleron M 900MHz (32 bits)
- Mémoire vive : 512Mo
- Stockage : 8GB SSD
- Ecran 7" 800x480
- 1 port ethernet format RJ45
- Wifi et bluetooth intégrés
- Lecteur de carte SD
- 3 ports USB 2.0
- 1 sortie VGA
- 1 entrée et 1 sortie audio analogique format minijack
- GPU Intel Mobile 910 GML

#### LOGICIELS :

- Distribution Debian GNU/Linux 11 (bullseye) i686 serveur +nonfree firmwares
- Serveur graphique : Xorg
- Gestionnaire de fenêtres : i3
- Editeur de texte format MarkDown avec prévisualisation : ReText (ou Ghostwriter)
- Convertisseur de documents : pandoc
- Editeur de pdf : qpdfview
- Gestionnaire de réseaux : network-manager-gnome
- Emulateur de terminal : LXTerminal
- Gestionnaire de fichiers : PCManFM
- Configuration des écrans : ARandR
- Personnaliser l'apparence applications GTK : LXAppearance
- Personnaliser l'apparence applications Qt5 : qt5ct

#### INSTALLATION :

- Télécharger l'iso debian 11 (bullseye), non officielles incluant les firmwares non-libres, pour architecture Intel 32 bits (i386), d'installation minimale par le réseau (netinstall) : https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/11.5.0+nonfree/i386/iso-cd/
- Flasher cette image sur un support de stockage (USB ou sur une carte SD), avec Etcher par exemple.
- Insérer le support et démarrer l'ordinateur, maintenir la touche ESC pour booter sur le support flashé.
- Suivre la procédure d'installation en laissant vide le mot de passe de l'utilisateur root en n'installant aucun environnement de bureau
- Redémarrer et s'identifier dans la console (tty), vérifier la connexion réseau
- Mettre à jour les paquets logiciels : sudo apt update puis sudo apt upgrade
- installer les paquets : sudo apt install xserver-xorg-core xserver-xorg-video-intel xinit i3
- Créer un fichier .xsession contenant :

setxkbmap fr

exec i3

- lancer le serveur graphique avec : startx
- configurer i3 : https://i3wm.org/docs/userguide.html 
- installer les derniers logiciels depuis la console (CTRL ALT F2) ou un émulateur de terminal (SUPER ENTER) :

sudo apt install retext qpdfview network-manager-gnome lxterminal lxappearance qt5ct pcmanfm arandr

### Mac mini 1,1 : Un poste bureautique léger ou un médiacenter

#### OBJECTIF :
Un poste bureautique léger avec différents exemples d'environnements de bureau, un mediacenter avec kodi

#### MATÉRIEL :
Mac mini 1,1 A1176 core duo 1,66, février 2006.

- coût d'origine 900€, en occasion 150€.
- CPU : Genuine Intel 1300 (2 coeurs) 1,66MHz (32 bits)
- Mémoire vive : 512Mo
- Stockage : 60GB disque dur
- 1 port video HDMI
- 1 port ethernet format RJ45
- Wifi et bluetooth intégré
- Lecteur de carte SD
- 4 ports USB 2.0
- 1 entrée et 1 sortie audio analogique format minijack, 1 sortie audio numérique toslink
- 1 port firewire
- GPU Intel Mobile 940 GML

#### LOGICIELS :

- Distribution Debian GNU/Linux 10 (buster) i686 serveur +nonfree firmwares
- Serveur graphique : Xorg
- Gestionnaire de fenêtres : openbox
- Environnements de bureau : LXDE, XFCE, MATE

#### INSTALLATION :
- Télécharger l'iso debian 10 (buster), non officielles incluant les firmwares non-libres, pour architecture Intel 32 bits (i386), d'installation minimale par le réseau (netinstall) : https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/archive/10.13.0+nonfree/i386/iso-cd/
- Flasher cette image sur un support de stockage (USB ou sur une carte SD), avec Etcher par exemple.
- Insérer le support et démarrer l'ordinateur, maintenir la touche ESC pour booter sur le support flashé.
- Suivre la procédure d'installation en laissant vide le mot de passe de l'utilisateur root en installant les environnements de bureau LXDE, XFCE et MATE
- Redémarrer et s'identifier dans le gestionnaire d'affichage
- Mettre à jour et installer des logiciels via le gestionnaire de paquets Synaptic ou via un émulateur de terminal : https://debian-facile.org/doc:systeme:apt:apt

### MISE EN ŒUVRE COLLECTIVE D'UN DES SCÉNARIOS : un médiacenter

#### OBJECTIF :
Un médiacenter à brancher sur un téléviseur ou moniteur de grande taille, un ampli audio avec une interface légère pour visionner des films, écouter de la musique, regarder des photos, streamer du contenu youtube, netflix, etc. 

#### MATÉRIEL :
Une machine virtuelle

#### LOGICIELS :
Distribution LibreElec 10.0 pour architecture 64bits (x86) : https://releases.libreelec.tv/LibreELEC-Generic.x86_64-10.0.2.img.gz 

#### INSTALLATION :

- Télécharger l'iso d'installation de la distribution LibreElec 10.0 pour architecture intel 64 bits (x86) : https://libreelec.tv/downloads/generic/

